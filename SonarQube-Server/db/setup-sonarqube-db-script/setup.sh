#!/bin/bash
#run the setup script to create the DB and the schema in the DB
#do this in a loop because the timing for when the SQL instance is ready is indeterminate
# $1 is SQL_SERVER_USERNAME argument passed from Dockerfile
# $2 is SQL_SERVER_PASSWORD argument passed from Dockerfile

tryLimit=10;
count=0;
finish=false;

username="$1"
password="$2"

initialDB() {
    /opt/mssql-tools/bin/sqlcmd -S localhost -U $username -P $password -i /sonarqube-initial-db/setup.sql
}


echo "........... start create initial SonarQube Database ..........."

while [[ $count -le $tryLimit ]]; do
    echo ">>>>>>>>>>>>> retry initial SonarQube Database counter" $count
    initialDB
    if [[ $? -eq 0 ]]; then
        echo "initial SonarQube Database Success ................"
        break
    else
        echo "initial SonarQube Database Fail !!!!!!!!!!!!!!"
        ((count=count+1));
        sleep 5
    fi
done