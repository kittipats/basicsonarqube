﻿

using System.Collections.Generic;

namespace WebApp.Models
{
    public class Class1
    {
        public decimal Calculate(decimal amount, int type, int years)
        {
            decimal result = 0;
            decimal disc = decimal.Zero;
            if (years > 5)
            {
                disc = (decimal)(5 / 10);
            }
            else
            {
                disc = (decimal)(years / 10);
            }

            if (type == 1)
            {
                result = amount;
            }
            else if (type == 2)
            {
                result = (amount - (0.1m * amount)) - disc * (amount - (0.1m * amount));
            }
            else if (type == 3)
            {
                result = (0.7m * amount) - disc * (0.7m * amount);
            }
            else if (type == 4)
            {
                result = (amount - (0.5m * amount)) - disc * (amount - (0.5m * amount));
            }
            return result;
        }
    }


    public class PasswordChangeModel
    {
        public string NewPassword { get; set; }
        public List<string> PasswordHistoryItems { get; set; }
        public string Username { get; set; }
    }

    public class PasswordValidator
    {
        private const int _minLength = 8;

        public bool IsValid(PasswordChangeModel passwordChangeModel)
        {
            if (passwordChangeModel.NewPassword.Length < _minLength ||
                passwordChangeModel.NewPassword == passwordChangeModel.Username ||
                (passwordChangeModel.PasswordHistoryItems != null &&
                passwordChangeModel.PasswordHistoryItems.Contains(passwordChangeModel.NewPassword)))
            {
                return false;
            }

            return true;
        }
    }

    public class PasswordManager
    {
        public bool ChangePassword()
        {
            var passwordValidator = new PasswordValidator();
            var isPasswordValid = passwordValidator.IsValid(new PasswordChangeModel());

            // the rest of the logic
            return false;
        }
    }
}
